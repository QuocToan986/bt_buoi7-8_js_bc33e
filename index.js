/*****************Hàm dom tới ô input***************************/
function getEl(input) {
  return document.querySelector(input);
}

/*****************Hàm lấy array******************/
var numArray = [];
function btnThemSo() {
  if (getEl("#txt-number").value.trim() == "") {
    return;
  }
  var number = getEl("#txt-number").value * 1;
  numArray.push(number);
  getEl("#txt-number").value = " ";
  getEl("#thongBao").innerText = numArray;
}

/*****************Bài 1**************************/
function tinhSoDuong(arr, index) {
  var tong = 0;
  if (arr[index] > 0) {
    tong += arr[index];
  }
  return tong;
}
function btnTinhTong() {
  for (var tong = 0, index = 0; index < numArray.length; index++) {
    tong += tinhSoDuong(numArray, index);
  }
  getEl("#thongBao1").innerHTML = `Tổng số dương: ${tong}`;
}

/*****************Bài 2**************************/
function demSoDuong(arr, index) {
  var count = 0;
  if (arr[index] > 0) {
    count++;
  }
  return count;
}
function btnDemSoDuong() {
  for (var count = 0, index = 0; index < numArray.length; index++) {
    count += demSoDuong(numArray, index);
  }
  getEl("#thongBao2").innerHTML = `Số dương đếm được: ${count}`;
}

/*****************Bài 3**************************/
function timSNN(arr, index) {
  var min = arr[0];
  if (min > arr[index]) {
    min = arr[index];
  }
  return min;
}
function btnTimSNN() {
  for (var index = 0; index < numArray.length; index++) {
    var min = timSNN(numArray, index);
  }
  getEl("#thongBao3").innerText = `Số nhỏ nhất trong mảng: ${min} `;
}

/*****************Bài 4**************************/
function timSDNN(arr, index) {
  var min = arr[0];
  if (min > arr[index] && arr[index] > 0) {
    min = arr[index];
  }
  return min;
}
function btnSoDuongNhoNhat() {
  for (var index = 0; index < numArray.length; index++) {
    var minDuong = timSDNN(numArray, index);
  }
  getEl("#thongBao4").innerHTML = ` Số dương nhỏ nhất trong mảng: ${minDuong} `;
}

/*****************Bài 5**************************/
function btnTimChanCuoi() {
  var chanCuoi = numArray[numArray.length - 1];
  if (chanCuoi % 2 === 0) {
    getEl("#thongBao5").innerHTML = `Số chẵn cuối của mảng: ${chanCuoi}`;
  } else {
    getEl("#thongBao5").innerHTML = "-1";
  }
}

/*****************Bài 6**************************/
function doiCho(viTri1, viTri2) {
  var temp = numArray[viTri1];
  numArray[viTri1] = numArray[viTri2];
  numArray[viTri2] = temp;
  return numArray;
}
function btnDoiCho() {
  var numArrayDoiCho = doiCho(
    getEl("#txt-vi-tri1").value,
    getEl("#txt-vi-tri2").value
  );
  getEl("#thongBao6").innerHTML = ` Mảng sau khi đổi chổ: ${numArrayDoiCho} `;
}

/*****************Bài 7**************************/
function btnSapXep() {
  numArray.sort(function (a, b) {
    return a - b;
  });
  getEl("#thongBao7").innerHTML = ` Mảng xắp xếp tăng dần: ${numArray} `;
}

/*****************Bài 8**************************/
function kiemTraSNT(number) {
  if (number < 2) {
    return !1;
  }
  for (var index = 2; index <= Math.sqrt(number); index++) {
    if (number % index == 0) {
      return !1;
    }
    return !0;
  }
}
function btnTimSoNguyen() {
  for (var n = -1, i = 0; i <= numArray.length; i++) {
    if (kiemTraSNT(numArray[i])) {
      n = numArray[i];
      break;
    }
  }
  getEl("#thongBao8").innerHTML = ` Số nguyên tố tìm được: ${n} `;
}

/*****************Bài 9**************************/
var arrsoThuc = [];
function btnThemSoThuc() {
  var soThucInput = getEl("#txt-so-thuc").value * 1;
  arrsoThuc.push(soThucInput);
  getEl("#thongBaoSoThuc").innerHTML = arrsoThuc;
}
function kiemTraSN(number) {
  var demInt = 0;
  for (var index = 0; index < number.length; index++) {
    if (Number.isInteger(number[index]) == true) {
      demInt++;
    }
  }
  return demInt;
}
function btnDemSoNguyen() {
  var demInt = 0;
  demInt = kiemTraSN(arrsoThuc);
  getEl("#thongBao9").innerHTML = ` Số nguyên đếm được: ${demInt} `;
}

/*****************Bài 10**************************/
function demDuongBai10(arr) {
  var demSoDuong = 0;
  for (var index = 0; index < arr.length; index++) {
    if (arr[index] > 0) {
      demSoDuong++;
    }
  }
  return demSoDuong;
}

function demSoAm(arr) {
  var demSoAm = 0;
  for (var index = 0; index < arr.length; index++)
    if (arr[index] < 0) {
      demSoAm++;
    }
  return demSoAm;
}
function btnSoSanh() {
  var soDuong = demDuongBai10(numArray);
  var soAm = demSoAm(numArray);
  if (soDuong > soAm) {
    getEl("#thongBao10").innerHTML = "số dương > số âm";
  } else if (soDuong < soAm) {
    getEl("#thongBao10").innerHTML = "số dương < số âm";
  } else {
    getEl("#thongBao10").innerHTML = "số dương = số âm";
  }
}
